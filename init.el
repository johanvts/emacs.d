;;Prefer UTF-8
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-language-environment "UTF-8")
;; When this fail 'rever-buffer-with-coding-system utf-8' => C-x RET r utf-8 RET
;; Treat clipboard input as UTF-8 string first; compound text next, etc.
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

;; Hide the GUI
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode 1))

;; Maximize the frame
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;;Custom settings
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

;;Initialize system variables
(defun system-win? ()
  "True if current system is Windows."
  (or (eq system-type 'windows-nt)
      (eq system-type 'ms-dos)))

(defun system-osx? ()
  "True if current system is Mac OSX."
  (eq system-type 'darwin))

(defun system-linux? ()
  "True if current system is Linux."
  (not (and (system-win?) (system-osx?))))

;;================================
;;        PACKAGE MANAGEMENT
;;================================

;; Set up the MELPA archive
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
	     '("org" . "https://orgmode.org/elpa/") t)
(package-initialize)

;; Install use-package if needed.
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Always ensure (install uninstalled packages)
(setq use-package-always-ensure t)

;; Enable pretty symbols with peeking.
(global-prettify-symbols-mode 1)

;; Auto-completion on TAB
(setq tab-always-indent 'complete)
(add-to-list 'completion-styles 'initials t)

;;=======================
;;        PACKAGES
;;=======================

(use-package helm
  :diminish helm-mode
  :bind
  ("M-x" . helm-M-x)
  ("C-x C-f" . helm-find-files)
  ("C-x f" . helm-find-files) ;;I don't think I have ever used fill column
  ("C-c k" . helm-show-kill-ring)
  ("C-c TAB" . helm-imenu)
  ("C-x b" . helm-buffers-list)
  :init
  (helm-mode 'true)
  (helm-autoresize-mode 'true)
  (add-hook 'LaTeX-mode-hook 'helm-mode)
  )

(use-package csharp-mode
  :init
  (add-hook 'csharp-mode-hook 'hl-line-mode)
  (add-hook 'csharp-mode-hook (lambda()(push '("<=" . ?≤) prettify-symbols-alist)))
  (add-hook 'csharp-mode-hook (lambda()(push '(">=" . ?≥) prettify-symbols-alist)))
  (add-hook 'csharp-mode-hook (lambda()(push '("=>" . ?⇒) prettify-symbols-alist)))
  (add-hook 'csharp-mode-hook (lambda()(push '("<=" . ?⇐) prettify-symbols-alist)))
  )

(use-package web-mode
  :bind
  (:map web-mode-map
	("TAB" . web-mode-fold-or-unfold))
)

(use-package markdown-mode
  :mode "\\.md\\'"
  :bind
  (:map markdown-mode-map
        ("M-<right>" . markdown-demote)
        ("M-<left>"  . markdown-promote))
  )

;;A cosy christmas
(use-package fireplace)

;;Speedbar
(require 'helm)
(use-package sr-speedbar
  :init
  (setq speedbar-show-unknown-files t)
  )

;;Powershell
(defun powershell (&optional buffer)
  "Launches a powershell in buffer *powershell* and switches to it."
  (interactive)
  (let ((buffer (or buffer "*PowerShell*"))
    (powershell-prog "C:\\Program Files\\PowerShell\\7-preview\\pwsh.exe"))
    (make-comint-in-buffer "powershell core" "*PowerShell*" powershell-prog)
    (switch-to-buffer buffer)))


;;Ace jump mode
(use-package ace-jump-mode
  :bind
  ("C-<return>" . ace-jump-mode)
  ("C-RET" . ace-jump-mode)
  )


;;Undo-tree mode
(use-package undo-tree
  :init
  (global-undo-tree-mode)
  )

(use-package ox-reveal
  :init
  (setq org-reveal-root "file:///c:/Users/johan.sivertsen/AppData/Roaming/.emacs.d/reveal.js-master")
  (setq org-reveal-title-slide nil)
  )

(use-package multiple-cursors
  :bind
  ("C-c m c" . mc/edit-lines)
  )

;;=======================
;; metapost png preview 
;;=======================

(load-file "~/.emacs.d/lisp/metapost-png-preview-mode.el")

;;=================
;; Winner mode
;;=================

(winner-mode)

;;=================
;;   elisp
;;=================

(define-key emacs-lisp-mode-map (kbd "C-c C-r") 'eval-region)

;;=================
;; SPELLING
;;=================

(setq ispell-program-name "C:/Program Files (x86)/Aspell/bin/aspell.exe")

;;=================
;;    XML
;;================

;;XML files are associated with xml-mode in 'auto-mode-alist'
;;That makes sense, so we will just remap xml-mode.
(defalias 'xml-mode 'web-mode 
  "Use `web-mode' instead of nXML's `xml-mode'.")

;;=================
;;    TARIF
;;=================

(add-to-list 'auto-mode-alist '("\\.TARIF\\'" . hexl-mode))

;;=================
;;     Theme
;;=================

(load-theme 'misterioso)


;;=================
;;      Org
;;=================

(use-package org
  :init
  (add-hook 'org-mode-hook 'turn-on-flyspell)
  :config
  (setq org-startup-with-inline-images t)
  (setq org-export-with-sub-superscripts nil)
  )


;;======================
;;  BONUS FUNCTIONS
;;======================

(defun toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
             (next-win-buffer (window-buffer (next-window)))
             (this-win-edges (window-edges (selected-window)))
             (next-win-edges (window-edges (next-window)))
             (this-win-2nd (not (and (<= (car this-win-edges)
                                         (car next-win-edges))
                                     (<= (cadr this-win-edges)
                                         (cadr next-win-edges)))))
             (splitter
              (if (= (car this-win-edges)
                     (car (window-edges (next-window))))
                  'split-window-horizontally
                'split-window-vertically)))
        (delete-other-windows)
        (let ((first-win (selected-window)))
          (funcall splitter)
          (if this-win-2nd (other-window 1))
          (set-window-buffer (selected-window) this-win-buffer)
          (set-window-buffer (next-window) next-win-buffer)
          (select-window first-win)
          (if this-win-2nd (other-window 1))))))

;;Insert Edlund commit message in correct format.
(defun insert-commit-message (comp subcomp summary jira desc review)
  (interactive "sComponent: \nsSub-Component: \nsSummary: \nsJira: \nsDescription: \nsReview: ")
  (insert (if (equal "" subcomp)
	  (format "%s: %s (#%s)\n\n%s\n\nReview: %s\n" comp summary jira desc review)
	  (format "%s, %s: %s(#%s)\n\n%s\n\nReview: %s\n" comp subcomp summary jira desc review)
	  )))

;;Move text with M-arrows (VS style)
(defun move-text-internal (arg)
   (cond
    ((and mark-active transient-mark-mode)
     (if (> (point) (mark))
            (exchange-point-and-mark))
     (let ((column (current-column))
              (text (delete-and-extract-region (point) (mark))))
       (forward-line arg)
       (move-to-column column t)
       (set-mark (point))
       (insert text)
       (exchange-point-and-mark)
       (setq deactivate-mark nil)))
    (t
     (beginning-of-line)
     (when (or (> arg 0) (not (bobp)))
       (forward-line)
       (when (or (< arg 0) (not (eobp)))
            (transpose-lines arg))
       (forward-line -1)))))

(defun move-text-down (arg)
   "Move region (transient-mark-mode active) or current line
  arg lines down."
   (interactive "*p")
   (move-text-internal arg))

(defun move-text-up (arg)
   "Move region (transient-mark-mode active) or current line
  arg lines up."
   (interactive "*p")
   (move-text-internal (- arg)))

;;Pretty xml

(require 'sgml-mode)

(defun reformat-xml ()
  (interactive)
  (save-excursion
    (sgml-pretty-print (point-min) (point-max))
    (indent-region (point-min) (point-max))))

;;=================
;;  Custom short cuts
;;=================
(global-set-key (kbd "M-<up>") 'move-text-up)
(global-set-key (kbd "M-<down>") 'move-text-down)
(global-set-key (kbd "M-m") 'insert-commit-message)
(global-set-key (kbd "C-c C-b") 'eval-buffer)
(global-set-key (kbd "C-<right>") 'toggle-window-split)
(global-set-key (kbd "C-<down>") 'delete-window)
(global-set-key (kbd "C-<up>") 'delete-other-windows)
(global-set-key (kbd "C-<prior>") 'beginning-of-buffer)
(global-set-key (kbd "C-<next>") 'end-of-buffer)
(global-set-key (kbd "C-s") 'isearch-forward)
(global-set-key (kbd "S-C-<left>") 'windmove-left)
(global-set-key (kbd "S-C-<right>") 'windmove-right)
(global-set-key (kbd "S-C-<up>") 'windmove-up)
(global-set-key (kbd "S-C-<down>") 'windmove-down)
(global-set-key (kbd "<f3>") 'comment-region)
(global-set-key (kbd "<f4>") 'uncomment-region)

(put 'erase-buffer 'disabled nil)