(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(current-language-environment "Swedish")
 '(mode-require-final-newline nil)
 '(package-selected-packages
   (quote
    (ox-reveal csharp-mode web-mode use-package multiple-cursors undo-tree sr-speedbar powershell helm fireplace ace-jump-mode)))
 '(prettify-symbols-unprettify-at-point (quote right-edge))
 '(reb-re-syntax (quote string))
 '(require-final-newline nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#2d3743" :foreground "#e1e1e0" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :family "Fira Code")))))
