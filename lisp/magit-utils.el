(add-to-list 'exec-path "C:/tools/cmder/vendor/msysgit/bin")

(global-set-key (kbd "C-x g") 'magit-status)

(setenv "GIT_ASKPASS" "git-gui--askpass")
